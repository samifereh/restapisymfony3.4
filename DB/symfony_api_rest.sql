-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 19 Octobre 2018 à 13:11
-- Version du serveur :  10.0.36-MariaDB-0ubuntu0.16.04.1
-- Version de PHP :  7.1.23-2+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `symfony_api_rest`
--

-- --------------------------------------------------------

--
-- Structure de la table `auth_tokens`
--

CREATE TABLE `auth_tokens` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `auth_tokens`
--

INSERT INTO `auth_tokens` (`id`, `user_id`, `value`, `created_at`) VALUES
(1, 1, 'd9fcQsiNBcfYHn6bWghv4WCZ9G7wYwzudO6oikzNLmzwb+hrBvgKzjizaKZM3DM2IWY=', '2018-10-12 17:45:20'),
(3, 1, 'f4OUyiFVvn456LeVlyAVwW9qRiMdCGj/ox9Bh/J5ZsW4V46UWr80goFsoDPXMzMtoTM=', '2018-10-15 13:40:42'),
(4, 1, 'j/msHin20zTnrdVL17rBGOp42lhrcoZnlt/O4ooNNffRbJoui/9uPe1naemeFbuCKkQ=', '2018-10-15 15:15:22'),
(5, 1, 'UHMmEKOtir2b3qnTRrWq6iAlJG95xt8l8sNN/E1iN6SF5GMVuJCJMCJ0Gb1yjK8QZzI=', '2018-10-16 13:33:38'),
(6, 1, '/txDhvpuX4TlK0s5J28f7ACpANMpxF9vx9c3EzXQkJ+VyFuewEThc7qztehHlB1fVPk=', '2018-10-16 13:33:59');

-- --------------------------------------------------------

--
-- Structure de la table `place`
--

CREATE TABLE `place` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `place`
--

INSERT INTO `place` (`id`, `name`, `address`) VALUES
(1, 'Tour Eiffel', '5 Avenue Anatole France, 75007 Paris'),
(2, 'Mont-Saint-Michel', '50120 Le Mont-Saint-Michel'),
(3, 'Château de Versailles', 'Place d\'Armes, 78000 Versailles'),
(4, 'Disneyland Paris', '77777 Marne-la-Vallée'),
(5, 'Musée du Louvre', '799, rue de Rivoli, 75001 Paris');

-- --------------------------------------------------------

--
-- Structure de la table `preferences`
--

CREATE TABLE `preferences` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `preferences`
--

INSERT INTO `preferences` (`id`, `user_id`, `name`, `value`) VALUES
(1, 1, 'history', 4),
(2, 1, 'art', 4),
(3, 1, 'sport', 3),
(4, 2, 'sport', 3),
(6, 2, 'history', 4);

-- --------------------------------------------------------

--
-- Structure de la table `prices`
--

CREATE TABLE `prices` (
  `id` int(11) NOT NULL,
  `place_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `prices`
--

INSERT INTO `prices` (`id`, `place_id`, `type`, `value`) VALUES
(1, 1, 'less_than_12', 5.75),
(2, 5, 'less_than_12', 6),
(3, 5, 'for_all', 15);

-- --------------------------------------------------------

--
-- Structure de la table `themes`
--

CREATE TABLE `themes` (
  `id` int(11) NOT NULL,
  `place_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `themes`
--

INSERT INTO `themes` (`id`, `place_id`, `name`, `value`) VALUES
(1, 1, 'architecture', 7),
(2, 2, 'history', 3),
(3, 2, 'art', 7),
(4, 1, 'history', 6);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `email`, `password`) VALUES
(1, 'Sami', 'FAREH', 'samiferah@tritux.com', '$2y$12$TmuQhbty9XWxHTinfRI9L.k0.nqjgl/gUDpjuenntdyofIFCyTbmm'),
(2, 'Ef', 'Ghi', 'ef.ghi@test.local', '$2y$12$efNHi4jz4M8WSyqFiBeiS.phhTIQli3XTdB7HXvhnzmbnkfd.QtKO'),
(3, 'Jean', 'Dupont', 'JeanDupont@gmail.com', '$2y$12$rjagDueSrQ.jAAfiHLo.2e3LugMC/h1X6i54m1ikMHIBdB3jNRyG6'),
(4, 'EfG', 'GhiK', 'ef.ghik@test.local', '$2y$12$C2lASujVPy2XXrLtCeLxI./Px0E6w.HxxuDIpJDtlN9qQIdvAF36u'),
(5, 'EfGdf', 'GhiKfd', 'ef.ghC@test.local', '$2y$12$HLmag81S38FN1.cqr1rtMuXvkAz7IdJgO.HpDsZ58DZaeKSu3DmSC');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `auth_tokens`
--
ALTER TABLE `auth_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_tokens_value_unique` (`value`),
  ADD KEY `IDX_8AF9B66CA76ED395` (`user_id`);

--
-- Index pour la table `place`
--
ALTER TABLE `place`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `places_name_unique` (`name`);

--
-- Index pour la table `preferences`
--
ALTER TABLE `preferences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `preferences_name_user_unique` (`name`,`user_id`),
  ADD KEY `IDX_E931A6F5A76ED395` (`user_id`);

--
-- Index pour la table `prices`
--
ALTER TABLE `prices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `prices_type_place_unique` (`type`,`place_id`),
  ADD KEY `IDX_E4CB6D59DA6A219` (`place_id`);

--
-- Index pour la table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `themes_name_place_unique` (`name`,`place_id`),
  ADD KEY `IDX_154232DEDA6A219` (`place_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `auth_tokens`
--
ALTER TABLE `auth_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `place`
--
ALTER TABLE `place`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `preferences`
--
ALTER TABLE `preferences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `prices`
--
ALTER TABLE `prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `auth_tokens`
--
ALTER TABLE `auth_tokens`
  ADD CONSTRAINT `FK_8AF9B66CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `preferences`
--
ALTER TABLE `preferences`
  ADD CONSTRAINT `FK_E931A6F5A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `prices`
--
ALTER TABLE `prices`
  ADD CONSTRAINT `FK_E4CB6D59DA6A219` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`);

--
-- Contraintes pour la table `themes`
--
ALTER TABLE `themes`
  ADD CONSTRAINT `FK_154232DEDA6A219` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
